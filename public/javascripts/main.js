(function($){
    var jsonData =[
        {
        'name' : 'pranav',
        'score': 79
        } ,
        {
        'name' : 'Navin',
        'score': 71
        } ,
        {
        'name' : 'Priyanka',
        'score': 64
        } ,
        {
        'name' : 'Murali',
        'score': 53
        } ,
        {
        'name': 'Prakash',
        'score': 49
        }
    ] 
    
    function getJsonData(){
        $('.renderTable tr').each(function(i, row){
            if(i<5){                
                jsonData[i]['name'] = $(this).find('td:nth-child(2)').html();
                jsonData[i]['score'] = $(this).find('td:nth-child(3) input').val(); 
            }    
        })
    }
    
    
    
    function sortJson(data, key){
        return data.sort(function(a,b){
           var x = a[key];
           var y = b[key];
           return ((x>y) ? -1 : ((x < y)? 1: 0));
        });
    }
    
    var changedData = sortJson(jsonData, 'score')
    
    function renderData(changedData){
        var resultRenderData = [];
        changedData.forEach(function(record, index){
            var newTableGeneration =  '<tr class="row" id="row'+(index+1) +'" ><td class="col-md-2">'+(index+1)+'</td><td class="col-md-3">'+record.name+'</td><td class="col-md-2"><input type="text" value='+record.score+' data-sequence="'+index+'"></td></tr>';
            resultRenderData.push(newTableGeneration)
        });
        var lastRowSubmit = '<tr class="row" id="row6" data-sequence="6"><td class="col-md-2"></td><td class="col-md-3"></td><td class="col-md-2"><input class="btn btn-primary" type="button" value="submit" ></td></tr>';
        resultRenderData.push(lastRowSubmit);
        $(document).ready(function (){
          $('.renderTable').append(resultRenderData.join(''));
          getJsonData();
        });
        
    }
    renderData(changedData);
    
    $(document).on('click', 'input[type="button"]', function(){
        getJsonData();
        var changedData = sortJson(jsonData, 'score');
        $('.renderTable').empty();
        renderData(changedData);
    })

    
})(jQuery);